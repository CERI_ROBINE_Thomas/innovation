neural network type : DNN

version_name = lowercase
preprocessing_method = Word2Vec
vectors_size = 100
batch_size = 100
nb_epochs = 10
nb_neurones = [36, 64, 96]
dropout = [0.1, 0.1, 0.1]
learning_rate = 0.001
momentum = 0.9
optimizer = Adam
loss = CrossEntropyLoss
metrics = Accuracy
DataParallel(
  (module): DNNNeuralNetwork(
    (linear1): Linear(in_features=20000, out_features=36, bias=True)
    (linear2): Linear(in_features=36, out_features=64, bias=True)
    (linear3): Linear(in_features=64, out_features=96, bias=True)
    (linear4): Linear(in_features=96, out_features=10, bias=True)
    (relu): ReLU()
    (softmax): Softmax(dim=-1)
    (dropout1): Dropout(p=0.1, inplace=False)
    (dropout2): Dropout(p=0.1, inplace=False)
    (dropout3): Dropout(p=0.1, inplace=False)
    (norm1): BatchNorm1d(36, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (norm2): BatchNorm1d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (norm3): BatchNorm1d(96, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  )
)