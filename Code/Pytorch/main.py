#-*- coding: utf-8 -*-

# Pytorch
import torch
# Data Processing
from model import Model
# Logs
import logging
from datetime import datetime
# CUDA and system
import os, sys
import subprocess

################################################################################

logging.basicConfig(format='%(asctime)s %(message)s')

##################################################################### PARAMETERS

# CUDA for PyTorch (2 GPUs good, 4 GPUs better)
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# Data
version_name = 'baseline'  # baseline, clean, with_smileys, with_punctuations, with_lowercase.
preprocessing_method = 'Word2Vec'
input_dimension = 200  # Meta-data + largest comment size + margin for future inference.
output_dimension = 10
vectors_size = 100  # 100, 10 or 5
date = ''  # If you want to use a checkpoint.
# Neural Network
str_NN = 'DNN'  # CNN or DNN
str_optimizer = 'SGD'
str_loss = 'CrossEntropyLoss'
str_metrics = 'Accuracy'
use_checkpoint = False  # Do not forget to set it True if needed.
nb_epochs = 10
nb_neurones = [36, 64, 96]
dropouts = [0.1, 0.1, 0.1]
learning_rate = 0.001  # Only used when no checkpoint.
momentum = 0.9

########################################################################### BASH

# If does not exist yet
os.system('mkdir ../../Logs')
os.system('mkdir ../../Logs/Tensorboard')
os.system('mkdir ../../Logs/Tensorboard/' + str_NN)
os.system('mkdir ../../Logs/Parameters')
os.system('mkdir ../../Logs/Parameters/' + str_NN)
os.system('mkdir ../../Logs/Results')
os.system('mkdir ../../Logs/Results/' + str_NN)
os.system('mkdir ../../Models')
os.system('mkdir ../../Models/' + str_NN)

########################################################################### LOGS

if not(use_checkpoint):
    # Wrtiting parameters
    date = datetime.now().strftime('%Y%m%d-%H%M%S')
    f = open('../../Logs/Parameters/' + str_NN + '/' + version_name + '_' + date + '.txt','a')
    f.write('neural network type : ' + str_NN + '\n')
    f.write('\nversion_name = ' + str(version_name))
    f.write('\npreprocessing_method = ' + str(preprocessing_method))
    f.write('\nvectors_size = ' + str(vectors_size))
    f.write('\nbatch_size = ' + str(100))
    f.write('\nnb_epochs = ' + str(nb_epochs))
    f.write('\nnb_neurones = ' + str(nb_neurones))
    f.write('\ndropout = ' + str(dropouts))
    f.write('\nlearning_rate = ' + str(learning_rate))
    f.write('\nmomentum = ' + str(momentum))
    f.write('\noptimizer = ' + str_optimizer)
    f.write('\nloss = ' + str_loss)
    f.write('\nmetrics = ' + str_metrics)
    f.close()

########################################################################### MAIN

model = Model(date, use_checkpoint,
            version_name, preprocessing_method, str_NN,
            vectors_size, input_dimension, output_dimension,
            nb_epochs, learning_rate, momentum, device, nb_neurones, dropouts)
model.run()
