#-*- coding: utf-8 -*-

# Pytorch
import torch
import torch.nn as nn

################################################################################

# CNN class
class CNNNeuralNetwork(nn.Module):

    # Initialize parameters
    def __init__(self, vectors_size, input_dimension, output_dimension, nb_neurones, dropouts):
        super(CNNNeuralNetwork,  self).__init__()
        # Convolutional layers
        self.conv1 = nn.Conv2d(1, nb_neurones[0], 3, 1)
        self.conv2 = nn.Conv2d(nb_neurones[0], nb_neurones[1], 3, 1)
        # Fully connected layers
        self.linear1 = nn.Linear((vectors_size-4) * nb_neurones[1] * (input_dimension-4), nb_neurones[2])
        self.linear2 = nn.Linear(nb_neurones[2], output_dimension)
        # Activation functions
        self.relu = nn.ReLU()
        self.softmax=nn.Softmax(dim=-1)
        # Dropouts
        self.dropout2d1 = nn.Dropout2d(dropouts[0])
        self.dropout2d2 = nn.Dropout2d(dropouts[1])
        self.dropout1d = nn.Dropout(dropouts[2])
        # Normalization
        self.norm2d1 = nn.BatchNorm2d(nb_neurones[0])
        self.norm2d2 = nn.BatchNorm2d(nb_neurones[1])
        self.norm1d = nn.BatchNorm1d(nb_neurones[2])

    # Define layers and functions
    def forward(self, x):
        #print(x.shape)
        # CNN layers
        x = self.conv1(x)
        x = self.relu(x)
        x = self.norm2d1(x)
        x = self.dropout2d1(x)
        #print(x.shape)
        x = self.conv2(x)
        x = self.relu(x)
        x = self.norm2d2(x)
        x = self.dropout2d2(x)
        #print(x.shape)
        # Reshape
        x = x.view(x.shape[0], -1)
        #print(x.shape)
        # Linear layer - Hidden
        x = self.linear1(x)
        x = self.relu(x)
        x = self.norm1d(x)
        x = self.dropout1d(x)
        #print(x.shape)
        # Linear layer - Output
        x = self.linear2(x)
        x = self.softmax(x)
        #print(x.shape)
        return x

# DNN class
class DNNNeuralNetwork(nn.Module):

    # Initialize parameters
    def __init__(self, vectors_size, input_dimension, output_dimension, nb_neurones, dropouts):
        super(DNNNeuralNetwork, self).__init__()
        # Fully connected layers
        self.linear1 = nn.Linear(vectors_size * input_dimension, nb_neurones[0])
        self.linear2 = nn.Linear(nb_neurones[0], nb_neurones[1])
        self.linear3 = nn.Linear(nb_neurones[1], nb_neurones[2])
        self.linear4 = nn.Linear(nb_neurones[2], output_dimension)
        # Activation functions
        self.relu = nn.ReLU()
        self.softmax=nn.Softmax(dim=-1)
        # Dropout
        self.dropout1 = nn.Dropout(dropouts[0])
        self.dropout2 = nn.Dropout(dropouts[1])
        self.dropout3 = nn.Dropout(dropouts[2])
        # Normalization
        self.norm1 = nn.BatchNorm1d(nb_neurones[0])
        self.norm2 = nn.BatchNorm1d(nb_neurones[1])
        self.norm3 = nn.BatchNorm1d(nb_neurones[2])

    # Define layers and functions
    def forward(self, x):
        #print(x.shape)
        x = torch.flatten(x, start_dim=1)
        #print(x.shape)
        # Fully connected layers
        x = self.linear1(x)
        x = self.relu(x)
        x = self.norm1(x)
        x = self.dropout1(x)
        x = self.linear2(x)
        x = self.relu(x)
        x = self.norm2(x)
        x = self.dropout2(x)
        x = self.linear3(x)
        x = self.relu(x)
        x = self.norm3(x)
        x = self.dropout3(x)
        #print(x.shape)
        # Linear layer - Output
        x = self.linear4(x)
        x = self.softmax(x)
        #print(x.shape)
        return x

########################################################################### MAIN

"""
import numpy as np
import torch.optim as optim

input_dimension = 3000  # window_size
output_dimension = 10
nb_neurones = [36, 64, 96]
dropouts = [0.1, 0.1, 0.1]
vectors_size = 10  # input_dimension
batch_size = 100
device = torch.device("cpu")

# CNN
CNNnet = CNNNeuralNetwork(vectors_size, output_dimension, nb_neurones, dropouts)
print(CNNnet)
optimizer = optim.Adam(CNNnet.parameters(), lr=0.1)
# x
x = torch.randn(batch_size, input_dimension, vectors_size)
x = x.clone().detach()
# Train
CNNnet.train()
optimizer.zero_grad()
out = CNNnet(x)
print(out.shape)
#print(out)

# DNN
DNNnet = DNNNeuralNetwork(vectors_size, input_dimension, output_dimension, nb_neurones, dropouts)
print(DNNnet)
optimizer = optim.Adam(DNNnet.parameters(), lr=0.1)
# x
x = torch.randn(batch_size, input_dimension, vectors_size)
x = x.clone().detach()
# Train
DNNnet.train()
optimizer.zero_grad()
out = DNNnet(x)
print(out.shape)
#print(out)
"""
