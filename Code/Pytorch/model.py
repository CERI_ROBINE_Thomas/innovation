# -*- coding: utf-8 -*-

# Pytorch
import torch
import torch.nn as nn
import torch.optim as optim
import torch.utils.data as data
from neuralNetwork import CNNNeuralNetwork, DNNNeuralNetwork
# Data Processing
import sys
import numpy as np
import _pickle as pickle
# Visualization
from torch.utils.tensorboard import SummaryWriter
# Logs
import logging

################################################################################

logging.basicConfig(format='%(asctime)s %(message)s')

# Build the model
class Model():

    # Initialization
    def __init__(self, date, use_checkpoint,
                version_name, preprocessing_method, str_NN,
                vectors_size, input_dimension, output_dimension,
                nb_epochs, learning_rate, momentum, device, nb_neurones, dropouts):
        # Files related
        self.checkpoint_path = '../../Models/' + str_NN + '/' + version_name + '_' + date + '.pth'
        self.result_path = '../../Logs/Results/' + str_NN + '/' + version_name + '_' + date + '.txt'
        self.parameters_path = '../../Logs/Parameters/' + str_NN + '/' + version_name + '_' + date + '.txt'
        self.writer = SummaryWriter('../../Logs/Tensorboard/' + str_NN + '/' + version_name + '_' + date)
        self.use_checkpoint = use_checkpoint
        # Data related
        self.version_name = version_name
        self.preprocessing_method = preprocessing_method
        self.vectors_size = vectors_size
        self.input_dimension = input_dimension
        self.output_dimension = output_dimension
        # Neural Network parameters
        self.nb_epochs = nb_epochs
        self.learning_rate = learning_rate
        self.device = device
        self.best_metric = 0
        self.str_NN = str_NN
        # Init NN
        self.init_NeuralNetwork(momentum, nb_neurones, dropouts)

    # Create the NN
    def init_NeuralNetwork(self, momentum, nb_neurones, dropouts):
        # Build NN
        if self.str_NN == 'CNN':
            self.net = nn.DataParallel(CNNNeuralNetwork(self.vectors_size, self.input_dimension, self.output_dimension, nb_neurones, dropouts))
        elif self.str_NN == 'DNN':
            self.net = nn.DataParallel(DNNNeuralNetwork(self.vectors_size, self.input_dimension, self.output_dimension, nb_neurones, dropouts))
        self.net.to(self.device)
        self.net = self.net.float()
        # If you want to resume a training
        if self.use_checkpoint:
            self.load_model()
        # If it's a new training
        else:
            f = open(self.parameters_path, 'a')
            f.write('\n' + str(self.net))
            f.close()
            self.optimizer = optim.SGD(self.net.parameters(), lr=self.learning_rate, momentum=momentum)  # Create a stochastic gradient descent optimizer.
        self.criterion = nn.CrossEntropyLoss()  # Create a loss function.

    # Transform dataset into tensors
    def get_tensors(self, batch):
        x = batch[0]
        y = batch[1]
        x = np.asarray(x).astype(np.float32)
        y = np.asarray(y).astype(np.float32)
        # From Numpy to Torch
        x = torch.from_numpy(x)
        y = torch.from_numpy(y)
        # Clone and Detach
        #x = x.clone().detach()
        #y = y.clone().detach()
        if self.str_NN == 'CNN':
            x = x.unsqueeze(1)
        # To GPU
        x = x.to(self.device)
        y = y.to(self.device)
        return x, y

    # Iterate through the model
    def iterate(self, dataset_index):
        # Initialize metrics
        self.epoch_loss = 0.0
        self.epoch_metric = 0.0
        total = 0
        nb_batches = 0
        end = False
        # For each batch
        while True:
            if nb_batches % 1000 == 0:
                logging.warning('Progress: ' + str(nb_batches) + ' batches processed')
            # Get Batches
            try:
                batch = pickle.load(self.pickle_file)  # Load a batches
            except EOFError:
                break  # No more batches to load
            # Make tensors
            x, y = self.get_tensors(batch)
            # Train
            if dataset_index == 0:
                self.optimizer.zero_grad()
                out = self.net(x)
                loss = self.criterion(out, torch.max(y,1)[1])  # CrossEntropyLoss do not accept a one-hot array but the index of the class.
                loss.backward()
                self.optimizer.step()
            # Evaluate
            elif dataset_index == 1:
                out = self.net(x)
                loss = self.criterion(out, torch.max(y,1)[1])
            #Loss
            self.epoch_loss += loss.item()
            # Metric
            y_pred = torch.max(out,1)[1]
            y_real = torch.max(y,1)[1]
            total += len(y_pred)
            self.epoch_metric += (y_pred == y_real).sum().item()
            nb_batches += 1
        # Get Loss and Metric
        self.epoch_loss /= nb_batches
        self.epoch_metric = float(self.epoch_metric) / float(total) * 100

    def inference(self):
        self.net.eval()
        self.load_model()
        nb_batches = 0
        end = False
        # For each batch
        while not(end):
            if nb_batches % 1000 == 0:
                logging.warning('Progress: ' + str(nb_batches) + ' batches processed')
            # Get Batches
            try:
                batch = pickle.load(self.pickle_file)  # Load a batches.
            except EOFError:
                break  # No more batches to load.
            # Make tensors and get keys
            x, y = self.get_tensors(batch)
            keys = batch[2]
            # Evaluate
            out = self.net(x)
            self.save_results(out.cpu().detach().numpy().astype(np.float32), keys)
            nb_batches += 1

    # Save the results of test corpus from inference
    def save_results(self, out, keys):
        n = 1  # Number of chunks of an array to make mean results.
        index = 0
        while index < len(keys)-1:
            if keys[index] == keys[index+1]:
                # Add results to make mean of results
                out[index] = np.add(out[index], out[index+1])
                out = np.delete(out, index+1, 0)
                keys = np.delete(keys, index+1, 0)
                n += 1
                index -= 1
            else:
                out[index] / n  # If not chunked, divided by 1. Else the sum is divided by the number of chunks.
                n = 1
            index += 1
        y_pred = np.argmax(out, axis=1).astype(np.float32)
        y_pred[y_pred == 0] = 0.5
        y_pred[y_pred == 1] = 1.0
        y_pred[y_pred == 2] = 1.5
        y_pred[y_pred == 3] = 2.0
        y_pred[y_pred == 4] = 2.5
        y_pred[y_pred == 5] = 3.0
        y_pred[y_pred == 6] = 3.5
        y_pred[y_pred == 7] = 4.0
        y_pred[y_pred == 8] = 4.5
        y_pred[y_pred == 9] = 5.0
        file = open(self.result_path, 'a')
        for index in range(len(y_pred)):
            file.write(keys[index] + ' ' + str(y_pred[index]).replace('.',',') + '\n')
        file.close()

    # Save the improoved model
    def save_model(self):
        state = {
        'state_dict': self.net.state_dict(),
        'optimizer': self.optimizer.state_dict(),
        'epoch': self.epoch_index + 1,
        'best_metric': self.best_metric,
        'learning_rate': self.learning_rate}
        torch.save(state, self.checkpoint_path)

    def load_model(self):
        checkpoint = torch.load(self.checkpoint_path, map_location='cuda')
        self.net.load_state_dict(checkpoint['state_dict'])
        self.optimizer.load_state_dict(checkpoint['optimizer'])
        self.epoch_index = checkpoint['epoch']
        self.best_metric = checkpoint['best_metric']
        self.learning_rate = checkpoint['learning_rate']

    # Run the training through epochs
    def run(self):
        # Iterate over nb_epochs
        for self.epoch_index in range(self.nb_epochs):
            logging.warning('Epoch ' + str(self.epoch_index + 1) + '/' + str(self.nb_epochs) + '\n')
            # Train
            self.pickle_file = open('../../Data/Pickle/train' + '_' + self.version_name + '_' + self.preprocessing_method + '_' + str(self.vectors_size) + '.p', 'rb')
            self.net.train()
            self.iterate(0)
            logging.warning('train_loss = ' + str(float(self.epoch_loss)) + ' - train_metric = ' + str(self.epoch_metric) + '\n')
            # Plot
            self.writer.add_scalar('Loss/train', self.epoch_loss, self.epoch_index)
            self.writer.add_scalar('Metric/train', self.epoch_metric, self.epoch_index)
            # Eval
            self.pickle_file = open('../../Data/Pickle/dev' + '_' + self.version_name + '_' + self.preprocessing_method + '_' + str(self.vectors_size) + '.p', 'rb')
            self.net.eval()
            self.iterate(1)
            logging.warning('dev_loss = ' + str(float(self.epoch_loss)) + ' - dev_metric = ' + str(self.epoch_metric) + '\n')
            # Plot
            self.writer.add_scalar('Loss/dev', self.epoch_loss, self.epoch_index)
            self.writer.add_scalar('Metric/dev', self.epoch_metric, self.epoch_index)
            # Save the model if better metric for dev
            if self.epoch_metric > self.best_metric:
                self.best_metric = self.epoch_metric
                self.save_model()
        # Test
        self.pickle_file = open('../../Data/Pickle/test' + '_' + self.version_name + '_' + self.preprocessing_method + '_' + str(self.vectors_size) + '.p', 'rb')
        self.inference()
