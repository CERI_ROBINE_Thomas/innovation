#!bin/bash

cd ../Kibana/Json/
split -d -a1 -b 100M --additional-suffix=.json Dev.json Dev_part
split -d -a2 -b 100M --additional-suffix=.json Train.json Train_part