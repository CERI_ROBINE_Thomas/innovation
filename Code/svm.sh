#!/bin/bash

cd Liblinear
make
./train -c 10 -e 0.1 ../Data/SVM/Train/with_smileys.svm ../Models/SVM/with_smileys.model
./train -c 10 -e 0.1 ../Data/SVM/Train/without_smileys.svm ../Models/SVM/without_smileys.model
./predict ../Data/SVM/Test/with_smileys.svm ../Models/SVM/with_smileys.model ../Logs/Results/SVM/with_smileys.txt
./predict ../Data/SVM/Test/without_smileys.svm ../Models/SVM/without_smileys/model ../Logs/Results/SVM/without_smileys.txt