#-*- coding: utf-8 -*-

import math
import json
from Json import load_json, get_words
from collections import Counter

# Get number of words "word" in one comment
def get_occ_comment(comment, word):
    occ = comment.count(word)
    return occ

# Get number of comments containing the word
def get_occ_comments(flat_comments, word):
    occ = flat_comments.count(word)
    return occ

# Get IDF for all words
def get_IDF(comments):
    words = get_words(comments)  # Uniq words
    flat_comments = [word for comment in comments for word in comment]
    print('Flat done')
    count_comments = Counter(flat_comments)
    print('Count done')
    IDF = {}
    for word in words:
        IDF[word] = math.log(len(flat_comments)/count_comments[word])
        print(IDF[word])
    return IDF

# Get TFIDF for one comment
def get_TFIDF(comment, IDF):
    TFIDF = {}
    for word in comment:
        tf = get_occ_comment(comment, word)/len(comment)
        try:
            idf = IDF[word]
        except KeyError:
            continue
        TFIDF[word] = tf*idf
    return TFIDF

# Save it one time to use it many times
def save_IDF():
    for version_name in ['baseline', 'clean']:
        # Get data
        comments, *_ = load_json(version_name, ['Train', 'Dev', 'Test'])
        print('Getting IDF')
        IDF = get_IDF(comments)
        # Save it
        print('Saving')
        file = open('../../Data/Json/IDF_' + version_name + '.json', 'w')
        json.dump(IDF,file)
        file.close()

# Load saved json file to preprocess actual data
def load_IDF(version_name):
    file = open('../../Data/Json/IDF_' + version_name + '.json', 'r')
    content = file.read()
    dict = json.loads(content)
    file.close()
    return dict

#load_IDF('baseline')
#save_IDF()
