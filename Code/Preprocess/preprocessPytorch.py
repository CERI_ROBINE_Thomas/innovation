#-*- coding: utf-8 -*-

# Data Processing
import numpy as np
import gc
from keras.utils import to_categorical
# Files Processing
from Gensim import load_vectors
from Json import load_json
import _pickle as pickle
# Pytorch
import torch
import torch.utils.data as data
# System
import os, sys
# Logs
import logging

################################################################################

logging.basicConfig(format='%(asctime)s %(message)s')

class Preprocessor():

    # Initialize parameters
    def __init__(self, dataset_name, version_name, preprocessing_method, input_dimension, vectors_size, batch_size):
        # Paths
        self.dataset_name = dataset_name
        self.version_name = version_name
        # Numerical
        self.input_dimension = input_dimension
        self.batch_size = batch_size
        self.vectors_size = vectors_size
        # Preprocess
        self.preprocessing_method = preprocessing_method
        # To load datasets
        self.data_preprocessed = 0  # Total number of comments loaded.
        self.x = []
        self.y = []
        self.keys = []
        # All info in Json
        self.comments = []
        self.directors = []
        self.nationalities = []
        self.rates = []
        self.review_ids = []
        self.usernames = []
        self.genres = []
        # Initialize all needed
        self.init()

    # Loads all needed in memory
    def init(self):
        self.comments, self.directors, self.nationalities, self.rates, self.review_ids, self.usernames, self.genres = load_json(self.version_name, [self.dataset_name])
        self.rates.extend([0.5,1.0,1.5,2.0,2.5,3.0,3.5,4.0,4.5,5.0])  # To be sure every class is represented (i).
        self.rates = np.asarray(self.rates)
        self.rates[self.rates == 5.0] = 9
        self.rates[self.rates == 4.5] = 8
        self.rates[self.rates == 4.0] = 7
        self.rates[self.rates == 3.5] = 6
        self.rates[self.rates == 3.0] = 5
        self.rates[self.rates == 2.5] = 4
        self.rates[self.rates == 2.0] = 3
        self.rates[self.rates == 1.5] = 2
        self.rates[self.rates == 1.0] = 1
        self.rates[self.rates == 0.5] = 0
        self.rates = to_categorical(self.rates)  # One-Hot encoding.
        self.rates = self.rates[:-10]  # Remove (i).
        self.IDF = None
        if self.version_name == 'clean':
            version_name_ref = 'clean'
        else :
            version_name_ref = 'baseline'
        self.preprocessing = load_vectors(self.preprocessing_method, self.vectors_size, version_name_ref)

    # Preprocess data to memory
    def preprocess_data(self):
        # Delete
        del self.x
        del self.y
        del self.keys
        gc.collect()
        # Allocate
        self.x = []
        self.y = []
        self.keys = []
        # Get Data
        if self.data_preprocessed < len(self.rates):
            index = self.data_preprocessed
            while index < self.data_preprocessed + self.batch_size and index < len(self.rates):
                # Data x
                self.x.extend(self.get_comment(index, self.comments[index]))
                # Data y
                self.y.extend([self.rates[index] for i in range(self.comments[index].shape[0])])
                # ID of the comment
                self.keys.extend([self.review_ids[index] for i in range(self.comments[index].shape[0])])
                index += 1
        # Switch from list to numpy array
        self.x = np.asarray(self.x)
        self.y = np.asarray(self.y)
        self.keys = np.asarray(self.keys)
        # Update self.data_preprocessed
        self.data_preprocessed += self.batch_size
        # Preprocess remaining
        if self.data_preprocessed < len(self.rates):
            return False
        # Finished preprocess for all dataset
        return True

    # Get meta data
    def get_meta(self, index):
        meta = []
        # Pad meta-data at vectors_size
        if len(self.directors[index]) < self.vectors_size:
            directors = np.pad(
            self.directors[index],
            (0, self.vectors_size-len(self.directors[index])),
            'wrap')
        else:
            directors = self.directors[index][:self.vectors_size]
        if len(self.nationalities[index]) < self.vectors_size:
            nationalities = np.pad(
            self.nationalities[index],
            (0, self.vectors_size-len(self.nationalities[index])),
            'wrap')
        else:
            nationalities = self.nationalities[index][:self.vectors_size]
        if len(self.genres[index]) < self.vectors_size:
            genres = np.pad(
            self.genres[index],
            (0, self.vectors_size-len(self.genres[index])),
            'wrap')
        else:
            genres = self.genres[index][:self.vectors_size]
        usernames = np.pad(
        [self.usernames[index]],
        (0, self.vectors_size-1),
        'wrap')
        return [directors, nationalities, usernames, genres]

    # Get preprocessed comment with Word2Vec / FastText
    def get_comment(self, index, comment):
        num_comment = []
        #meta = self.get_meta(index)
        # For each chunk
        for chunk_index in range(len(comment)):
            num_comment_chunk = []
            # For each word
            for word_index in range(self.input_dimension):
                try:
                    num_comment_chunk.append(eval(self.preprocessing[comment[chunk_index][word_index]]))
                except KeyError:
                    num_comment_chunk.append(np.zeros((self.vectors_size)))
                    continue
            num_comment.append(num_comment_chunk)
            #num_comment.append(meta.extend(num_comment_chunk))
        return num_comment

    # Make batches from datasets
    def get_batch(self, shuffle):
        # Preprocess a certain quantity of data
        end = self.preprocess_data()
        if end:
            self.data_preprocessed = 0
        # Shuffle
        if shuffle:
            permutation = np.random.permutation(len(self.x))
            self.x = self.x[permutation]
            self.y = self.y[permutation]
        # We do not need to keep keys attached, because they are only used during inference, where data is not shuffled
        return end

    def save_pickle(self):
        logging.warning(str(self.x.shape)+" "+str(self.y.shape)+" "+str(self.keys.shape))
        with open('../../Data/Pickle/' + self.dataset_name.lower() + '_' + self.version_name + '_' + self.preprocessing_method + '_' + str(self.vectors_size) + '.p', 'ab+') as file:
            pickle.dump([self.x, self.y, self.keys], file)


########################################################################### MAIN

os.system('mkdir ../../Data/Pickle')
version_names = ['clean'] #, 'baseline', 'lowercase', 'without_smileys', 'without_ponctuation'] # baseline, clean, with_smileys, with_punctuations, with_lowercase
preprocessing_methods = ['FastText'] #, 'Wodr2Vec']
vectors_size = 100
batch_size = 100
input_dimension = 200  # meta-data + largest comment size + margin for future inference
for version_name in version_names:
    logging.warning(version_name)
    for preprocessing_method in preprocessing_methods:
        logging.warning(preprocessing_method)
        shuffle = True
        for corpus in ['Train', 'Dev', 'Test']:
            logging.warning(corpus)
            end = False
            preprocessor = Preprocessor(corpus, version_name, preprocessing_method, input_dimension, vectors_size, batch_size)
            if corpus == 'Test':
                shuffle = False
            while not(end):
                end = preprocessor.get_batch(shuffle)
                preprocessor.save_pickle()
