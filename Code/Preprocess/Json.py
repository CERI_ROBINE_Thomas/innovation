#-*- coding: utf-8 -*-

import json
import nltk
import re
import unidecode
import numpy as np

nltk.download('punkt')

def load_json(version_name, dataset_name):
    # Get comments from json
    comments = []
    directors = []
    nationalities = []
    rates = []
    review_ids = []
    usernames = []
    genres = []
    print('Getting comments')
    for dataset_name in dataset_name:
        print(dataset_name)
        # Get file content
        file = open('../../Data/Json/' + dataset_name + '/' + version_name + '.json', 'r')
        content_string = file.read()
        content_array = [eval(object) for object in content_string.split('\n')[:-1]]
        file.close()
        for element in content_array:
            # Clean the text
            comment = element['comment']
            comment = unidecode.unidecode(comment)
            comments.append(comment)
            # Get other information
            directors.append(element['directors'])
            nationalities.append(element['nationalities'])
            rates.append(element['rate'])
            review_ids.append(element['review_id'])
            usernames.append(element['username'])
            genres.append(element['genres'])
        print(len(comments))
    print('Got every comment')
    # Get words, [nltk.word_tokenize(comment) for comment in comments] too long
    #comments = [comment.split(' ') for comment in comments]
    comments = [nltk.word_tokenize(comment) for comment in comments]
    # Took nltk stopwords.words('french'), added some
    stopwords = [
    'au', 'aux', 'avec', 'ce', 'ces', 'dans', 'de', 'des', 'du', 'elle', 'en',
    'et', 'eux', 'il', 'ils', 'je', 'la', 'le', 'les', 'leur', 'lui', 'ma', 'mais',
    'me', 'même', 'mes', 'moi', 'mon', 'ne', 'nos', 'notre', 'nous', 'on', 'ou',
    'par', 'pas', 'pour', 'qu', 'que', 'qui', 'sa', 'se', 'ses', 'son', 'sur',
    'ta', 'te', 'tes', 'toi', 'ton', 'tu', 'un', 'une', 'vos', 'votre', 'vous',
    'c', 'd', 'j', 'l', 'a', 'm', 'n', 's', 't', 'y', 'été', 'étée', 'étées',
    'étés', 'étant', 'étante', 'étants', 'étantes', 'suis', 'es', 'est', 'sommes',
    'êtes', 'sont', 'serai', 'seras', 'sera', 'serons', 'serez', 'seront', 'serais',
    'serait', 'serions', 'seriez', 'seraient', 'étais', 'était', 'étions', 'étiez',
    'étaient', 'fus', 'fut', 'fûmes', 'fûtes', 'furent', 'sois', 'soit', 'soyons',
    'soyez', 'soient', 'fusse', 'fusses', 'fût', 'fussions', 'fussiez', 'fussent',
    'ayant', 'ayante', 'ayantes', 'ayants', 'eu', 'eue', 'eues', 'eus', 'ai', 'as',
    'avons', 'avez', 'ont', 'aurai', 'auras', 'aura', 'aurons', 'aurez', 'auront',
    'aurais', 'aurait', 'aurions', 'auriez', 'auraient', 'avais', 'avait',
    'avions', 'aviez', 'avaient', 'eut', 'eûmes', 'eûtes', 'eurent', 'aie', 'aies',
    'ait', 'ayons', 'ayez', 'aient', 'eusse', 'eusses', 'eût', 'eussions',
    'eussiez', 'eussent', 'leurs', 'vos', 'votre', 'votres', 'notres', 'elles',
    'mêmes', 'donc', 'or', 'ni', 'car', 'parce', 'voila', 'voici', 'fait',
    'faire', 'ca', 'cette', 'cet', 'etc', 'si', 'quelle', 'quel', 'sinon',
    'quels', 'quelles', 'quoi', 'quand', 'quant', 'alors', 'au', 'aucun',
    'aucuns', 'aussi', 'autre', 'avant', 'apres', 'avoir', 'cela', 'ci',
    'ceux', 'chaque', 'comme', 'comment', 'dedans', 'dehors', 'depuis', 'debut',
    'dors', 'encore', 'faites', 'font', 'hors', 'ici', 'juste', 'maintenant',
    'mien', 'tien', 'sien', 'meme', 'plupart', 'plusieurs', 'seulement', 'sous',
    'tandis', 'tels', 'tel', 'tous', 'tout', 'toute', 'toutes', 'vu', 'voir',
    'voient']
    # Remove Stop Words
    print('Removing stopwords')
    nn = 0
    for index in range(len(comments)):
        comment = [word for word in comments[index] if word.lower() not in stopwords]
        if (len(comment)%200) != 0:
            comment = np.pad(comment,(0, 200-(len(comment)%200)), 'wrap')
        elif len(comment) == 0:
            comment = np.pad(['a'],(0,199), 'wrap')  # Word not existing to pad -> 0.0
        chunked_comment = np.asarray(comment).reshape((-1, 200))
        if chunked_comment.shape[0] != 1:
            nn += 1
        comments[index] = chunked_comment
    print(nn)
    #print(len(max(comments, key=len)))
    return comments, directors, nationalities, rates, review_ids, usernames, genres

def get_words(comments):
    flat_comments = [word for comment in comments for word in comment]
    words = list(dict.fromkeys(flat_comments))
    return words

#load_json('baseline',['Train','Dev','Test'])
