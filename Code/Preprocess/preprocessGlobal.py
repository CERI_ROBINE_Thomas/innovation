# -*- coding:utf-8 -*-

### Imports

import json
import re
import unidecode
import subprocess
import os
from os import walk
import emoji
from emoji import UNICODE_EMOJI
import numpy as np

### Data

ColonSmileys = [':)', ':(', ':/', ":'(", ":')", ':p', ':P', ':D', ":-)", ':-(', ':-D', ':-P', ':-p']
SemiColonSmileys = [';)', ';(', ';/', ";'(", ";')", ';p', ';P', ';D', ";-)", ';-(', ';-D', ';-P', ';-p']
EqualSmileys =  ['=)', '=(', '=/', "='(", "=')", '=p', '=P', '=D', "=-)", '=-(', '=-D', '=-P', '=-p']
CircumflexSmileys = ['^^', '^^\'']
XSmileys = ['xp', 'xP', 'x)', 'x/', "x'p", "x'P", "x')", 'xd', 'xD', "x'd", "x'D", 'x(', "x'(", 'XP', "X'P", 'X)', "X')", 'XD', "X'D", 'X(', "X'("]
SlashSmileys = [':/', ';/', '=/', 'x/']
SadSmileys = [':(', ":'(", ':-(', ';(', ";'(", ';-(', '=(', "='(", '=-(', 'x(', "x'(", 'X(', "X'("]
HappySmileys = [':)', ":')",  ":-)", ';)', ";')", ";-)", '=)', "=')", "=-)", 'x)', "x')", 'X)', "X')"]
SimpleQuoteSmileys = [":'(", ":')", ";'(", ";')", "='(", "=')", '^^\'', "x'p", "x'P", "x')", "x'd", "x'D", "x'(", "X'P", "X')", "X'D", "X'("]

marks_indexes = {"0,5": 0,
		  		 "1,0": 1,
		  		 "1,5": 2,
		  		 "2,0": 3,
		  		 "2,5": 4,
		  		 "3,0": 5,
	  			 "3,5": 6,
		  		 "4,0": 7,
		  		 "4,5": 8,
		  		 "5,0": 9} 

### Functions

def createMoviesDictAndURLS(trainXML, devXML, testXML, dict_movies_path, movies_urls_path):
	''' (String, String, String, String, String) -> NoneType

	createMoviesDictAndURLS("../../Data/XML/train.xml", "../../Data/XML/dev.xml", "../../Data/XML/test.xml", "../../Data/Json/dict_movies.json", "../../Data/TXT/movies_urls.txt")
	>>> {
	  "221540": {
	    "director": [], 
	    "nationality": [], 
	    "genre": [], 
	    "ratingValue": 0}
	}
	https://www.allocine.fr/film/fichefilm_gen_cfilm=244430.html
	'''
	dict_movies = {}
	movie_id = ''
	movies_txt = open(movies_urls_path, "w")
	xml = open(trainXML)
	for line in xml:
		# get movie_id
		if "<movie>" in line:
			movie_id = line.split('>')[1]
			movie_id = movie_id.split('<')[0]
			if movie_id not in dict_movies:
				dict_movies[movie_id] = {
										"director": [],
										"nationality": [],
										"genre": [],
										"ratingValue": 0}
				movies_txt.write("https://www.allocine.fr/film/fichefilm_gen_cfilm="+movie_id+".html\n")
	xml.close()
	xml = open(devXML)
	for line in xml:
		# get movie_id
		if "<movie>" in line:
			movie_id = line.split('>')[1]
			movie_id = movie_id.split('<')[0]
			if movie_id not in dict_movies:
				dict_movies[movie_id] = {
										"director": [],
										"nationality": [],
										"genre": [],
										"ratingValue" :0}
				movies_txt.write("https://www.allocine.fr/film/fichefilm_gen_cfilm="+movie_id+".html\n")
	xml.close()
	xml = open(testXML)
	for line in xml:
		# get movie_id
		if "<movie>" in line:
			movie_id = line.split('>')[1]
			movie_id = movie_id.split('<')[0]
			if movie_id not in dict_movies:
				dict_movies[movie_id] = {
										"director": [],
										"nationality": [],
										"genre": [],
										"ratingValue": 0}
				movies_txt.write("https://www.allocine.fr/film/fichefilm_gen_cfilm="+movie_id+".html\n")
	xml.close()
	movies_txt.close()
	with open(dict_movies_path, 'w') as fp:
		json.dump(dict_movies, fp, indent=2)

def fillMoviesDict(dict_movies_path, urls_directory_path):
	''' (String, String) -> NoneType

	fillMoviesDict("../../Data/Json/dict_movies.json", "../../Data/Urls/www.allocine.fr/film/")
	>>> {
	  "221540": {
	    "director": [
	      "Rose Bosch"
	    ], 
	    "nationality": [
	      "français"
	    ], 
	    "genre": [
	      "Comédie dramatique"
	    ], 
	    "ratingValue": 3.7
	  }
	}
	'''
	dict_movies = {}
	movie_id = ''
	with open(dict_movies_path) as json_file:
		dict_movies = json.load(json_file) 
	for (repertoire, sousRepertoires, fichiers) in walk(urls_directory_path):
		for fichier in fichiers:
			movie_id = fichier.split('=')[1]
			movie_id = movie_id.split('.')[0]
			movie = open(urls_directory_path+fichier)
			genre = []
			note = 0
			nationalite = []
			realisateur = []
			end_of_html = False
			recording_multi_genre = False
			recording_solo_director = False
			recording_multi_director = False
			for row in movie:
				# on obtient la/les nationalité(s) du film
				if '= nationality">' in row:
					row = row.split('= nationality">')[1]
					row = row.split('\n')[0]
					row = row.split(',')[0]
					row = row.split('</span>')[0]
					nationalite.append(row.split(' ')[1])
				# on obtient la note globale du film
				if '"ratingValue":' in row:
					row = row.split('"ratingValue":')[1]
					row = row.split(',\n')[0]
					row = row.split('"')[1]
					row = row.split(',')
					note = float(row[0]+'.'+row[1])
				# condition d'arret pour l'obtention des genres du film
				if ']' in row and recording_multi_genre:
					recording_multi_genre = False
				# on obtient tous les genres du film
				if recording_multi_genre:
					row = row.split('\n')[0]
					row = row.split(',')[0]
					genre.append(row.split('"')[1])
				# on obtient et l'on stoppe la récupération de l'unique réalisateur
				if recording_solo_director and 'name' in row:
					row = row.split('"name":')[1]
					row = row.split('\n')[0]
					realisateur.append(row.split('"')[1])
					recording_solo_director = False 
				# condition d'arret pour l'obtention des réalisateurs du film
				if ']' in row and recording_multi_director:
					recording_multi_director = False
				# récupération des réalisateurs du film
				if 'name' in row and recording_multi_director:
					row = row.split('"name":')[1]
					row = row.split('\n')[0]
					realisateur.append(row.split('"')[1])
				# condition pour être sur que l'on se situe dans la bonne partie du document
				if end_of_html:
					# condition de début pour récupérer le(s) réalisateur(s)
					if '"director": {\n' in row:
						recording_solo_director = True
					elif '"director": [' in row:
						recording_multi_director = True
					# condition de début pour récupérer le(s) genre(s) du film
					if '"genre": [' in row and end_of_html:
						recording_multi_genre = True
					# récupération de l'unique genre du film
					elif '"genre":' in row and end_of_html:
						row = row.split('"genre":')[1]
						row = row.split('\n')[0]
						genre.append(row.split('"')[1])
				# condition d'activation pour être sur que l'on se situe dans la bonne partie du document	
				if '<script type="application/ld+json">' in row:
					end_of_html = True
			movie.close()
			dict_movies[movie_id]["director"] = realisateur
			dict_movies[movie_id]["nationality"] = nationalite
			dict_movies[movie_id]["genre"] = genre
			dict_movies[movie_id]["ratingValue"] = note
	with open(dict_movies_path,'w') as fp:
		json.dump(dict_movies, fp ,ensure_ascii=False, indent=2)

def createDirectorsNationalitiesGenresFiles(movies_dict_path, output_file_path_directors, output_file_path_nationalities, output_file_path_genres):
	directors = []
	directors.append('No director')
	nationalities = []
	nationalities.append('No nationality')
	genres = []
	genres.append('No genre')
	with open(movies_dict_path,'r') as f:
  		movies_dict = json.load(f)
	for key in movies_dict:
		for director in movies_dict[key]['director']:
			if director not in directors:
				directors.append(director)
		for nationality in movies_dict[key]['nationality']:
			if nationality not in nationalities:
				nationalities.append(nationality)
		for genre in movies_dict[key]['genre']:
			if genre not in genres:
				genres.append(genre)
	writer = open(output_file_path_directors, 'w')
	for director in directors:
		writer.write(director+'\n')
	writer = open(output_file_path_nationalities, 'w')
	for nationality in nationalities:
		writer.write(nationality+'\n')
	writer = open(output_file_path_genres, 'w')
	for genre in genres:
		writer.write(genre+'\n')

def createUsernamesFile(train_path, dev_path, test_path, output_file_path):
	usernames = []
	username = ''
	xml = open(train_path)
	for elem in xml:
		if "<name>" in elem:
			username = getLineInfo(elem)
			username = username.split('\n')[0]
			if username not in usernames:
				usernames.append(username)
	xml = open(dev_path)
	for elem in xml:
		if "<name>" in elem:
			username = getLineInfo(elem)
			username = username.split('\n')[0]
			if username not in usernames:
				usernames.append(username)
	xml = open(test_path)
	for elem in xml:
		if "<name>" in elem:
			username = getLineInfo(elem)
			username = username.split('\n')[0]
			if username not in usernames:
				usernames.append(username)
	writer = open(output_file_path, 'w')
	for name in usernames:
		writer.write(name+'\n')

def getMarkIndex(mark):
	return marks_indexes[mark]

def getWordIndex(word, words_dict):
	return words_dict[word]

def getReverseMarkIndex(index):
	for mark in marks_indexes:
		if marks_indexes[mark] == index:
			return mark

def startNewComment(fileToWrite, corpus, index):
	''' (file, String, int) -> NoneType

	startNewComment(Dev.json, dev, 1)
	>>> {"index":{"_index":"data_movie_'+dev+'","_id":'+str(1)+'}}
		{
	'''
	fileToWrite.write('{"index":{"_index":"data_movie_'+corpus+'","_id":'+str(index)+'}}\n')
	fileToWrite.write('{')

def getLineInfo(line):
	''' (String) -> String

	getLineInfo("<note>3,5</note>")
	>>> "3,5"
	'''
	return line.split('>')[1].split('<')[0]

def getMovieInfo(dico, index):
	''' (Dict, String) -> list of String, float, list of String, list of String

	getMovieInfo(movies_dict, "266711")
	>>> ["Drame", "Comédie", "Comédie dramatique"], 2.3, ["français"], ["Rebecca Zlotowski"]
	'''
	return dico[index]["genre"], dico[index]["ratingValue"], dico[index]["nationality"], dico[index]["director"]

def getCommentBegin(line):
	''' (String) -> String

	getCommentBegin("<commentaire>Énorme ce film, parmi les meilleurs biopics/documentaires, que l’on ait pu réaliser.\n")
	>>> "Énorme ce film, parmi les meilleurs biopics/documentaires, que l’on ait pu réaliser. "
	'''
	return line.split('<commentaire>')[1].split('\n')[0]+" "

def getCommentPart(line):
	''' (String) -> String

	getCommentPart("Un scénario qui a été parfaitement recopié point par point.\n")
	>>> "Un scénario qui a été parfaitement recopié point par point. "
	'''
	return line.split('\n')[0]+" "

def getCommentEnd(line):
	''' (String) -> String

	getCommentEnd("Mise en scène et cadrages puissants et marquants. Je le déconseille aux moins de 10 ans. 4/5</commentaire>")
	>>> "Mise en scène et cadrages puissants et marquants. Je le déconseille aux moins de 10 ans. 4/5"
	'''
	return line.split('</commentaire')[0]

def getCommentFull(line):
	''' (String) -> String

	getCommentFull("<commentaire>A voir.</commentaire>")
	>>> "A voir."
	'''
	return line.split('<commentaire>')[1].split('</commentaire>')[0]

def symbolToName(comment, symbol, name):
	''' (String, character, String) -> String

	symbolToName("la coquette somme de 10 000 $", '$', "dollars")
	>>> "la coquette somme de 10 000 dollars"
	'''
	commentParts = comment.split(symbol)
	comment = ""
	for index in range(len(commentParts)):
		if index == len(commentParts)-1:
			comment += commentParts[index]
		else:
			comment += commentParts[index]+" "+name+" "
	return comment

def getCommentWithout(comment, character):
	''' (String, character) -> String

	getCommentWithout("Autant que l'enquête de police qui aurait dû s'ensuivre ! Raté !", '!')
	>>> "Autant que l'enquête de police qui aurait dû s'ensuivre Raté "
	'''
	commentParts = comment.split(character) # tokenization
	comment = ""
	for index in range(len(commentParts)):
		if index == len(commentParts)-1:
			comment += commentParts[index]
		else:
			comment += commentParts[index]+" "
	return comment

def getCommentWithoutSpace(comment):
	''' (String) -> String

	getCommentWithoutSpace("parlent avec un langage soutenu,        spoiler:        son petit ami ne sert à que dalle, a part gagner 15 minutes     et un sur jeu de Julie")
	>>> "parlent avec un langage soutenu, spoiler: son petit ami ne sert à que dalle, a part gagner 15 minutes et un sur jeu de Julie"
	'''
	commentParts = comment.split(' ') # tokenization
	comment = ""
	for part in commentParts:
		if part != "":
			if comment == "":
				comment += part
			else:
				comment += " "+part
	return comment

def getRightNumber(parts):
	''' (list of String) -> list of String

	getSuspensionPointsRightNumber(["Bonjour", "", "", "comment", "allez-vous", "?"])
	>>> ["Bonjour", "", "", "", "comment", "allez-vous", "?"]
	'''
	indexes = []
	begin = False
	begin_index = 0
	between_points = 0
	for index in range(len(parts)):
		if begin and parts[index] != "":
			if between_points > 0:
				indexes.append(begin_index+1)
				between_points = 0
			begin = False
		if begin and parts[index] == "":
			between_points += 1
		if not begin and parts[index] != "":
			begin = True
			begin_index = index
	for index in reversed(indexes):
		parts.insert(index, "")
	return parts

def getWordKeepingSuspensionPoints(word):
	''' (String) -> String

	getWordKeepingSuspensionPoints("Bonjour...comment allez-vous ?")
	>>> "Bonjour ... comment allez-vous ? "
	'''
	parts = word.split('.') # tokenization
	word = ""
	previous_part = ""
	parts = getRightNumber(parts)
	for part in parts:
		if part != "":
			if previous_part == ".":
				word += " "+part+" "
			else:
				word += part+" "
		else:
			word += "."
			previous_part = "."
	return word

def getCommentKeepingSuspensionPoints(comment):
	''' (String) -> String

	getCommentKeepingSuspensionPoints("Bonjour...C.Donald...comment allez-vous ?")
	>>> "Bonjour ... C Donald ... comment allez-vous ?"
	'''
	commentParts = comment.split(" ") # tokenization
	comment = ""
	for part in commentParts:
		if ".." in part:
			comment += getWordKeepingSuspensionPoint(part)
		else:
			comment += getCommentWithout(part, '.')
	return comment

def getCommentLowercase(comment):
	''' (String) -> String

	getCommentLowercase("HELLO")
	>>> "hello"
	'''
	return comment.lower()

def getHTTPEnd(part):
	''' (String) -> String

	getHTTPEnd("://cinedidou.blog4ever.com  Cataleya est une jeune fille de 9 ans")
	>>> "://cinedidou.blog4ever.com"
	'''
	stop = [')', ' ']
	url = ""
	for character in part:
		if character in stop:
			return url
		else:
			url += character
	return part

def getCommentWithoutHTTP(comment, tag):
	''' (String, String) -> String

	getCommentWithoutHTTP("Http://cinedidou.blog4ever.com  Cataleya est une jeune fille de 9 ans", "Http")
	>>> "Cataleya est une jeune fille de 9 ans"
	'''
	Https = []
	commentParts = comment.split(tag)
	if len(commentParts) > 1:
		for index in range(1, len(commentParts)):
			Https.append(getHTTPEnd(commentParts[index]))
		for Http in Https:
			parts = comment.split(tag+Http)
			comment = ""
			for part in parts:
				comment += part+" "
	return comment

def deleteSmileysVerification(comment, smiley):
	''' (String, String) -> String

	deleteSmileysVerification("Plus le même réalisateur =plus les mêmes acteurs = suite d'un premier film souvent désastreux =p", "=p")
	>>> "Plus le même réalisateur  =plus les mêmes acteurs = suite d'un premier film souvent désastreux"
	'''
	commentParts = comment.split(smiley)
	comment = ""
	for index in range(len(commentParts)):
		if index == len(commentParts)-1:
			comment += commentParts[index]
		else:
			comment += commentParts[index]
			if commentParts[index+1] != "" and commentParts[index+1][0] != ' ' and commentParts[index+1][0].islower():
				comment += smiley
	return comment

def deleteSmileys(comment, smileys):
	''' (String, list of string) -> String

	deleteSmileys("tous ceux qui ont un coeur de midinette. ^^", ['^^', "^^'"])
	>>> "tous ceux qui ont un coeur de midinette. "
	'''
	specials = [':', ';', '=', 'x', 'X']
	for smiley in smileys:
		if smiley[0] in specials:
			comment = deleteSmileysVerification(comment, smiley)
		else:
			comment = getCommentWithout(comment, smiley)
	return comment

def getCommentWithoutSmileys(comment):
	comment = deleteSmileys(comment, ColonSmileys)
	comment = deleteSmileys(comment, SemiColonSmileys)
	comment = deleteSmileys(comment, EqualSmileys)
	comment = deleteSmileys(comment, CircumflexSmileys)
	comment = deleteSmileys(comment, XSmileys)
	comment = getCommentWithoutEmojis(comment)
	comment = getCommentWithoutSpace(comment)
	return comment

def processCommentWithoutSmileys(comment):
	''' (String) -> String

	processCommentWithoutPonctuation("Amateur de film de possession, j'ai été extrêmement déçu. 🎬 
	Le 1er Anabelle etait vraiment génial. Le 2ème n'ai vraiment pas à la hauteur. Très peu de scène flippantes, l'histoire n'est pas terrible. Plus le même réalisateur =plus les mêmes acteurs = suite d'un premier film souvent désastreux. Passer votre chemin.")
	>>> "Amateur de film de possession j'ai été extrêmement déçu Le 1er Anabelle etait vraiment génial Le 2ème n'ai vraiment pas à la hauteur. Très peu de scène flippantes l'histoire n'est pas terrible Plus le même réalisateur =plus les mêmes acteurs = suite d'un premier film souvent désastreux. Passer votre chemin"
	'''
	comment = getCommentWithout(comment, '\n')
	comment = getCommentWithout(comment, '\r')
	comment = getCommentWithout(comment, '&gt;')
	comment = getCommentWithoutHTTP(comment, 'http')
	comment = getCommentWithoutHTTP(comment, 'Http')
	comment = getCommentWithoutSmileys(comment)
	comment = getCommentWithout(comment, '.')	
	comment = getCommentWithout(comment, '!')
	comment = getCommentWithout(comment, '?')
	comment = getCommentWithout(comment, ',')
	comment = getCommentWithout(comment, ';')
	comment = getCommentWithout(comment, ':')
	comment = getCommentWithout(comment, '/')
	comment = getCommentWithout(comment, '\\')
	comment = getCommentWithout(comment, '#')
	comment = getCommentWithout(comment, '%')
	comment = getCommentWithout(comment, '*')
	comment = getCommentWithout(comment, '"')
	comment = getCommentWithout(comment, '‘')
	comment = getCommentWithout(comment, '’')
	comment = getCommentWithout(comment, '&amp') # &
	comment = getCommentWithout(comment, '«')
	comment = getCommentWithout(comment, '»')
	comment = getCommentWithout(comment, '…')
	comment = symbolToName(comment, '$', "dollars")
	comment = symbolToName(comment, '€', "euros")
	comment = getCommentWithoutSpace(comment)
	return comment

def getCommentWithSmileys(comment, smileys, character, index):
	''' (String, list of String, character, int) -> String

	getCommentWithSmileysColon("Quel régal ce film ! ; Je vous le conseille vraiment ;)", 
							[';)', ';(', ';/', ";'(", ";')", ';p', ';P', ';D', ";-)", ';-(', ';-D', ';-P', ';-p'], ';', 0)
	>>> "Quel régal ce film ! Je vous le conseille vraiment :)"
	'''
	if index < len(smileys):
		commentParts = comment.split(smileys[index])
		comment = ""
		for part_index in range(len(commentParts)):
			if part_index == len(commentParts)-1:
				comment += getCommentWithSmileys(commentParts[part_index], smileys, character, index+1)
			else:
				comment += getCommentWithSmileys(commentParts[part_index], smileys, character, index+1)+" "+smileys[index]+" "
	else:
		comment = getCommentWithout(comment, character)
	return comment

def getCommentLowercase(comment):
	''' (String) -> String

	getCommentLowercase("HELLO")
	>>> "hello"
	'''
	return comment.lower()

def getCommentBaseline(comment):
	''' (String) -> String

	processCommentWithoutPonctuation("Amateur de film de possession, j'ai été extrêmement déçu. 🎬 
	Le 1er Anabelle etait vraiment génial. Le 2ème n'ai vraiment pas à la hauteur. Très peu de scène flippantes, l'histoire n'est pas terrible. Plus le même réalisateur =plus les mêmes acteurs = suite d'un premier film souvent désastreux. Passer votre chemin.")
	>>> "Amateur de film de possession j'ai été extrêmement déçu Le 1er Anabelle etait vraiment génial Le 2ème n'ai vraiment pas à la hauteur. Très peu de scène flippantes l'histoire n'est pas terrible Plus le même réalisateur =plus les mêmes acteurs = suite d'un premier film souvent désastreux. Passer votre chemin"
	'''
	comment = getCommentWithout(comment, '\n')
	comment = getCommentWithout(comment, '\r')
	comment = getCommentWithout(comment, '&gt;') # chevron
	comment = getCommentWithoutHTTP(comment, 'http')
	comment = getCommentWithoutHTTP(comment, 'Http')
	comment = getCommentKeepingSuspensionPoints(comment)
	comment = splitWord(comment, '!')
	comment = splitWord(comment, '?')
	comment = getCommentWithout(comment, '/!\\')
	comment = getCommentWithSmileys(comment, ColonSmileys, ':', 0)
	comment = getCommentWithSmileys(comment, SemiColonSmileys, ';', 0)
	comment = getCommentWithSmileys(comment, SlashSmileys, '/', 0)
	comment = getCommentWithSmileys(comment, HappySmileys, ')', 0)
	comment = getCommentWithSmileys(comment, SadSmileys, '(', 0)
	comment = getCommentWithSmileys(comment, SimpleQuoteSmileys, "'", 0)
	comment = getCommentWithout(comment, '[')
	comment = getCommentWithout(comment, ']')
	comment = getCommentWithout(comment, '{')
	comment = getCommentWithout(comment, '}')
	comment = getCommentWithout(comment, ',')
	comment = getCommentWithout(comment, '\\')
	comment = getCommentWithout(comment, '#')
	comment = getCommentWithout(comment, '%')
	comment = getCommentWithout(comment, '*')
	comment = getCommentWithout(comment, '"')
	comment = getCommentWithout(comment, '‘')
	comment = getCommentWithout(comment, '’')
	comment = getCommentWithout(comment, '&amp') # &
	comment = getCommentWithout(comment, '«')
	comment = getCommentWithout(comment, '»')
	comment = symbolToName(comment, '$', "dollars")
	comment = symbolToName(comment, '€', "euros")
	comment = getCommentWithoutSpace(comment)
	return comment

def processKibana(XMLpath, outputfileName, corpus, movies_dict_path):
	''' (String, String, String, dict, String) -> NoneType

	xmlToJson("../../Data/Raw/dev.xml", "../../Data/Json/Dev.json", "dev", "../../Data/Json/dict_movies.json")
	>>> {"index":{"_index":"data_movie_dev","_id":1}}
{"user_id":"Z20150411212834470532236","movie_id":"244430","realisateur": [{"text":"Bourlem Guerdjou"}],"nationalite": [{"text":"français"}],"note globale":4.0,"note":5.0,"commentaire":"Ce téléfilm est tout simplement bouleversant Je ne sais pas comment expliquer le malaise que j'ai ressenti en voyant le calvaire de cette gamine à l'école J'avais déjà vu des reportages où sa maman parlait du cas de sa fille Marion qui s'était donné la mort à cause du harcèlement scolaire et j'avais hâte de mettre des images sur ce fait divers tragique et bien je n'ai pas été déçu Le film est bouleversant révoltant et j'ai eu du mal à regarder parfois tant l'acharnement injuste et puéril envers cette petite m'a choqué Les réactions du corps enseignant des parents d'élève tout m'a indigné et on ne peut pas ressortir indemne d'un tel film Une pépite à montrer à tous pour réfléchir et prévenir nos enfants que de tels comportements sont intolérables et condamnables","review_id":"review_57113853","username":"I'm A Rocket Man","lst_mots": [{"text":"Ce"},{"text":"téléfilm"},{"text":"est"},{"text":"tout"},{"text":"simplement"},{"text":"bouleversant"},{"text":"Je"},{"text":"ne"},{"text":"sais"},{"text":"pas"},{"text":"comment"},{"text":"expliquer"},{"text":"le"},{"text":"malaise"},{"text":"que"},{"text":"j'ai"},{"text":"ressenti"},{"text":"en"},{"text":"voyant"},{"text":"le"},{"text":"calvaire"},{"text":"de"},{"text":"cette"},{"text":"gamine"},{"text":"à"},{"text":"l'école"},{"text":"J'avais"},{"text":"déjà"},{"text":"vu"},{"text":"des"},{"text":"reportages"},{"text":"où"},{"text":"sa"},{"text":"maman"},{"text":"parlait"},{"text":"du"},{"text":"cas"},{"text":"de"},{"text":"sa"},{"text":"fille"},{"text":"Marion"},{"text":"qui"},{"text":"s'était"},{"text":"donné"},{"text":"la"},{"text":"mort"},{"text":"à"},{"text":"cause"},{"text":"du"},{"text":"harcèlement"},{"text":"scolaire"},{"text":"et"},{"text":"j'avais"},{"text":"hâte"},{"text":"de"},{"text":"mettre"},{"text":"des"},{"text":"images"},{"text":"sur"},{"text":"ce"},{"text":"fait"},{"text":"divers"},{"text":"tragique"},{"text":"et"},{"text":"bien"},{"text":"je"},{"text":"n'ai"},{"text":"pas"},{"text":"été"},{"text":"déçu"},{"text":"Le"},{"text":"film"},{"text":"est"},{"text":"bouleversant"},{"text":"révoltant"},{"text":"et"},{"text":"j'ai"},{"text":"eu"},{"text":"du"},{"text":"mal"},{"text":"à"},{"text":"regarder"},{"text":"parfois"},{"text":"tant"},{"text":"l'acharnement"},{"text":"injuste"},{"text":"et"},{"text":"puéril"},{"text":"envers"},{"text":"cette"},{"text":"petite"},{"text":"m'a"},{"text":"choqué"},{"text":"Les"},{"text":"réactions"},{"text":"du"},{"text":"corps"},{"text":"enseignant"},{"text":"des"},{"text":"parents"},{"text":"d'élève"},{"text":"tout"},{"text":"m'a"},{"text":"indigné"},{"text":"et"},{"text":"on"},{"text":"ne"},{"text":"peut"},{"text":"pas"},{"text":"ressortir"},{"text":"indemne"},{"text":"d'un"},{"text":"tel"},{"text":"film"},{"text":"Une"},{"text":"pépite"},{"text":"à"},{"text":"montrer"},{"text":"à"},{"text":"tous"},{"text":"pour"},{"text":"réfléchir"},{"text":"et"},{"text":"prévenir"},{"text":"nos"},{"text":"enfants"},{"text":"que"},{"text":"de"},{"text":"tels"},{"text":"comportements"},{"text":"sont"},{"text":"intolérables"},{"text":"et"},{"text":"condamnables"}],"genre": [{"text":"Drame"}]}
	'''
	comment_counter = 1
	word_counter = 0
	overall_mark = 0
	movie_id = ""
	previous_movie_id = ""
	review_id = ""
	username = ""
	user_id = ""
	mark = ""
	comment = ""
	nationalities = []
	directors = []
	genres = []
	complete_comment = True
	movies_dict = {}
	with open(movies_dict_path,'r') as f:
  		movies_dict = json.load(f)
	output_json_file = open(outputfileName,"w")
	xml = open(XMLpath)
	for elem in xml:
		# nouveau commentaire
		if elem == "<comment>\n":
			startNewComment(output_json_file, corpus, comment_counter)
			comment_counter += 1
		# movie_id
		if "<movie>" in elem:
			movie_id = getLineInfo(elem)
			# si il s'agit d'un commentaire au sujet d'un film different que le commentaire precedent
			if movie_id != previous_movie_id:
				genres, overall_mark, nationalities, directors = getMovieInfo(movies_dict, movie_id)
				previous_movie_id = movie_id
		# review_id
		if "<review_id>" in elem:
			review_id = getLineInfo(elem)
		# username
		if "<name>" in elem:
			username = getLineInfo(elem)
		# user_id
		if "used_id" in elem:
			user_id = getLineInfo(elem)
		# note
		if "<note>" in elem:
			mark = getLineInfo(elem).split(',')
		# récupérer l'intégralité des commentaires comportant un/plusieurs saut(s) de ligne
		if complete_comment == False:
			# fin du commentaire ?
			if "</commentaire>" in elem:
				comment += getCommentEnd(elem)
				complete_comment = True
			# le commentaire continue
			else:
				comment += getCommentPart(elem)
		# commentaire
		if "<commentaire>" in elem:
			# commentaire complet
			if "</commentaire>" in elem:
				comment = getCommentFull(elem)
			# le commentaire continue sur d'autres lignes
			else:
				comment = getCommentBegin(elem)
				complete_comment = False
		# traitement du commentaire à la fin de la balise "comment", lorsque l'on a recupérer tout le commentaire
		if elem == "</comment>\n":
			comment = processCommentWithoutSmileys(comment)
			# liste_mots
			words_list = comment.split(' ')
			# ajout des informations
			output_json_file.write('"user_id":"'+user_id+
				'","movie_id":"'+movie_id+
				'","realisateurs": [')
			if len(directors) > 0:
				for director in directors: 
					if word_counter == len(directors)-1:
						output_json_file.write('{"text":"'+director+'"}],')
					else:
						output_json_file.write('{"text":"'+director+'"},')
					word_counter += 1
				word_counter = 0
			else:
				output_json_file.write('{"text":""}],')
			output_json_file.write('"nationalites": [')
			if len(nationalities) > 0:
				for nationality in nationalities:
					if word_counter == len(nationalities)-1:
						output_json_file.write('{"text":"'+nationality+'"}],')
					else:
						output_json_file.write('{"text":"'+nationality+'"},')
					word_counter += 1
				word_counter = 0
			else:
				output_json_file.write('{"text":""}],')
			output_json_file.write('"note globale":'+str(overall_mark)+
				',"note":'+str(mark[0])+'.'+str(mark[1])+
				',"commentaire":"'+comment+'","review_id":"'+review_id+
				'","username":"'+username+
				'","lst_mots": [')
			for word in words_list:
				if word_counter == len(words_list)-1:
					output_json_file.write('{"text":"'+word+'"}],')
				else:
					output_json_file.write('{"text":"'+word+'"},')
				word_counter += 1
			word_counter = 0
			output_json_file.write('"genres": [')
			if len(genres) > 0:
				for genre in genres:
					if word_counter == len(genres)-1:
						output_json_file.write('{"text":"'+genre+'"}]}\n')
					else:
						output_json_file.write('{"text":"'+genre+'"},')
					word_counter += 1
				word_counter = 0
			else:
				output_json_file.write('{"text":""}]}\n')
	xml.close()
	output_json_file.close()

def xmlToSVM(train, dev, test, train_svm_path, dev_svm_path, test_svm_path, mode):
	''' (String, String, String, String, String, String, String) -> NoneType

	xmlToSVM("../../Data/Raw/train.xml", "../../Data/Raw/dev.xml", "../../Data/Raw/test.xml", "../../Data/SVM/Train/", "../../Data/SVM/Dev/", "../../Data/SVM/Test/", "With")
	>>> 6 5:1 8:1 10:1 27:1 33:1 61:1 106:1 111:1 143:1 148:1 176:1 185:1 261:1 263:1 428:1 473:1 508:1 695:1 740:1 1437:1 2188:1 4821:1 195709:1
	'''
	mark = ""
	comment = ""
	complete_comment = True
	words_dict = {}
	word_count = 1
	writer = open(train_svm_path+mode.lower()+'_smileys.svm', "w")
	xml = open(train)
	for elem in xml:
		# note
		if "<note>" in elem:
			mark = getLineInfo(elem)
		# récupérer l'intégralité des commentaires comportant un/plusieurs saut(s) de ligne
		if complete_comment == False:
			# fin du commentaire ?
			if "</commentaire>" in elem:
				comment += getCommentEnd(elem)
				complete_comment = True
			# le commentaire continue
			else:
				comment += getCommentPart(elem)
		# commentaire
		if "<commentaire>" in elem:
			# commentaire complet
			if "</commentaire>" in elem:
				comment = getCommentFull(elem)
			# le commentaire continue sur d'autres lignes
			else:
				comment = getCommentBegin(elem)
				complete_comment = False
		# traitement du commentaire à la fin de la balise "comment", lorsque l'on a recupérer tout le commentaire
		if elem == "</comment>\n":
			comment_dict = {}
			if mode == "With":
				comment = getCommentBaseline(comment)
			else:
				comment = processCommentWithoutSmileys(comment)
			comment = comment.split(' ')
			for word in comment:
				if word not in words_dict:
					words_dict[word] = word_count
					word_count += 1
				word_index = getWordIndex(word, words_dict)
				if word_index not in comment_dict:
					comment_dict[word_index] = 1
				else:
					comment_dict[word_index] += 1
			comment_dict = dict(sorted(comment_dict.items(), key = lambda t: t[0]))
			writer.write(str(getMarkIndex(mark)))
			for index in comment_dict:
				writer.write(' '+str(index)+':'+str(comment_dict[index]))
			writer.write('\n')
	writer = open(dev_svm_path+mode.lower()+'_smileys.svm', "w")
	xml = open(dev)
	for elem in xml:
		# note
		if "<note>" in elem:
			mark = getLineInfo(elem)
		# récupérer l'intégralité des commentaires comportant un/plusieurs saut(s) de ligne
		if complete_comment == False:
			# fin du commentaire ?
			if "</commentaire>" in elem:
				comment += getCommentEnd(elem)
				complete_comment = True
			# le commentaire continue
			else:
				comment += getCommentPart(elem)
		# commentaire
		if "<commentaire>" in elem:
			# commentaire complet
			if "</commentaire>" in elem:
				comment = getCommentFull(elem)
			# le commentaire continue sur d'autres lignes
			else:
				comment = getCommentBegin(elem)
				complete_comment = False
		# traitement du commentaire à la fin de la balise "comment", lorsque l'on a recupérer tout le commentaire
		if elem == "</comment>\n":
			comment_dict = {}
			if mode == "With":
				comment = processCommentWithSmileys(comment)
			else:
				comment = processCommentWithoutSmileys(comment)
			comment = comment.split(' ')
			for word in comment:
				if word not in words_dict:
					words_dict[word] = word_count
					word_count += 1
				word_index = getWordIndex(word, words_dict)
				if word_index not in comment_dict:
					comment_dict[word_index] = 1
				else:
					comment_dict[word_index] += 1
			comment_dict = dict(sorted(comment_dict.items(), key = lambda t: t[0]))
			writer.write(str(getMarkIndex(mark)))
			for index in comment_dict:
				writer.write(' '+str(index)+':'+str(comment_dict[index]))
			writer.write('\n')
	writer = open(test_svm_path+mode.lower()+'_smileys.svm', "w")
	xml = open(test)
	for elem in xml:
		# récupérer l'intégralité des commentaires comportant un/plusieurs saut(s) de ligne
		if complete_comment == False:
			# fin du commentaire ?
			if "</commentaire>" in elem:
				comment += getCommentEnd(elem)
				complete_comment = True
			# le commentaire continue
			else:
				comment += getCommentPart(elem)
		# commentaire
		if "<commentaire>" in elem:
			# commentaire complet
			if "</commentaire>" in elem:
				comment = getCommentFull(elem)
			# le commentaire continue sur d'autres lignes
			else:
				comment = getCommentBegin(elem)
				complete_comment = False
		# traitement du commentaire à la fin de la balise "comment", lorsque l'on a recupérer tout le commentaire
		if elem == "</comment>\n":
			comment_dict = {}
			if mode == "With":
				comment = processCommentWithSmileys(comment)
			else:
				comment = processCommentWithoutSmileys(comment)
			comment = comment.split(' ')
			for word in comment:
				if word not in words_dict:
					words_dict[word] = word_count
					word_count += 1
				word_index = getWordIndex(word, words_dict)
				if word_index not in comment_dict:
					comment_dict[word_index] = 1
				else:
					comment_dict[word_index] += 1
			comment_dict = dict(sorted(comment_dict.items(), key = lambda t: t[0]))
			writer.write(str(0))
			for index in comment_dict:
				writer.write(' '+str(index)+':'+str(comment_dict[index]))
			writer.write('\n')

def resultToGoodFormat(test_result, test_corpus, output_file_path):
	''' (String, String, String) -> NoneType

	resultToGoodFormat("../../Logs/Results/SVM/with_smileys.txt", "../../Data/XML/test.xml", "../../Logs/Results/SVM/gf_with_smileys.txt")
	>>> review_59354742 2,5
	'''
	predictions = []
	count = 0
	review_id = ""
	writer = open(output_file_path, 'w')
	results = open(test_result)
	for cipher in results:
		predictions.append(cipher)
	corpus = open(test_corpus)
	for elem in corpus:
		if "<review_id>" in elem:
			review_id = getLineInfo(elem)
		if elem == "</comment>\n":
			mark = int(predictions[count])
			mark = getReverseMarkIndex(mark)
			writer.write(review_id+' '+mark+'\n')
			count += 1

def getIndex(elem, list_path):
	lst = []
	file = open(list_path)
	for row in file:
		lst.append(row.split('\n')[0])
	return lst.index(elem)

def splitWord(comment, character):
	comment_parts = comment.split(character)
	comment = ""
	for i in range(len(comment_parts)):
		if i == len(comment_parts)-1:
			if comment_parts[i] != "":
				comment += " "+comment_parts[i]
		else:
			if comment_parts[i] != "":
				comment += " "+comment_parts[i]+" "
			comment += character
	return comment

def processBaseline(XMLpath, outputfileName, movies_dict_path, directors_path, nationalities_path, usernames_path, genres_path):
	''' (String, String, String, dict, String) -> NoneType

	xmlToJson("../Data/Raw/dev.xml", "../Data/Json/Dev.json", "dev", "../Data/Json/dict_movies.json")
	>>> {"index":{"_index":"data_movie_dev","_id":1}}
{"realisateur": [{"text":"Bourlem Guerdjou"}],"nationalite": [{"text":"français"}],"note":5.0,"commentaire":"Ce téléfilm est tout simplement bouleversant Je ne sais pas comment expliquer le malaise que j'ai ressenti en voyant le calvaire de cette gamine à l'école J'avais déjà vu des reportages où sa maman parlait du cas de sa fille Marion qui s'était donné la mort à cause du harcèlement scolaire et j'avais hâte de mettre des images sur ce fait divers tragique et bien je n'ai pas été déçu Le film est bouleversant révoltant et j'ai eu du mal à regarder parfois tant l'acharnement injuste et puéril envers cette petite m'a choqué Les réactions du corps enseignant des parents d'élève tout m'a indigné et on ne peut pas ressortir indemne d'un tel film Une pépite à montrer à tous pour réfléchir et prévenir nos enfants que de tels comportements sont intolérables et condamnables","review_id":"review_57113853","username":"I'm A Rocket Man","genre": [{"text":"Drame"}]}
	'''
	movie_id = ""
	previous_movie_id = ""
	review_id = ""
	username = ""
	mark = ""
	comment = ""
	nationalities = []
	directors = []
	genres = []
	complete_comment = True
	movies_dict = {}
	with open(movies_dict_path,'r') as f:
  		movies_dict = json.load(f)
	output_json_file = open(outputfileName,"w")
	xml = open(XMLpath)
	for elem in xml:
		# movie_id
		if "<movie>" in elem:
			movie_id = getLineInfo(elem)
			# si il s'agit d'un commentaire au sujet d'un film different que le commentaire precedent
			if movie_id != previous_movie_id:
				genres, overall_mark, nationalities, directors = getMovieInfo(movies_dict, movie_id)
				previous_movie_id = movie_id
		# review_id
		if "<review_id>" in elem:
			review_id = getLineInfo(elem)
		# username
		if "<name>" in elem:
			username = getLineInfo(elem)
		# note
		if "<note>" in elem:
			mark = getLineInfo(elem).split(',')
		# récupérer l'intégralité des commentaires comportant un/plusieurs saut(s) de ligne
		if complete_comment == False:
			# fin du commentaire ?
			if "</commentaire>" in elem:
				comment += getCommentEnd(elem)
				complete_comment = True
			# le commentaire continue
			else:
				comment += getCommentPart(elem)
		# commentaire
		if "<commentaire>" in elem:
			# commentaire complet
			if "</commentaire>" in elem:
				comment = getCommentFull(elem)
			# le commentaire continue sur d'autres lignes
			else:
				comment = getCommentBegin(elem)
				complete_comment = False
		# traitement du commentaire à la fin de la balise "comment", lorsque l'on a recupérer tout le commentaire
		if elem == "</comment>\n":
			username = username.split('\n')[0]
			comment = getCommentBaseline(comment)
			output_json_file.write('{"directors": [')
			if len(directors) > 0:
				for i in range(len(directors)): 
					if i == len(directors)-1:
						output_json_file.write(str(getIndex(directors[i], directors_path))+'],')
					else:
						output_json_file.write(str(getIndex(directors[i], directors_path))+',')
			else:
				output_json_file.write('0],')
			output_json_file.write('"nationalities": [')
			if len(nationalities) > 0:
				for i in range(len(nationalities)):
					if i == len(nationalities)-1:
						output_json_file.write(str(getIndex(nationalities[i], nationalities_path))+'],')
					else:
						output_json_file.write(str(getIndex(nationalities[i], nationalities_path))+',')
			else:
				output_json_file.write('0],')
			output_json_file.write('"rate":'+str(mark[0])+'.'+str(mark[1])+
				',"comment":"'+comment+'","review_id":"'+review_id+
				'","username":'+str(getIndex(username, usernames_path))+',"genres": [')
			if len(genres) > 0:
				for i in range(len(genres)):
					if i == len(genres)-1:
						output_json_file.write(str(getIndex(genres[i], genres_path))+']}\n')
					else:
						output_json_file.write(str(getIndex(genres[i], genres_path))+',')
			else:
				output_json_file.write('0]}\n')
	xml.close()
	output_json_file.close()

def processLowerize(dict_path, output_file_path):
	print('Lowerize')
	file = open(dict_path, 'r')
	content_string = file.read()
	content_array = [eval(object) for object in content_string.split('\n')[:-1]]
	file.close()
	writer = open(output_file_path, 'w')
	for element in content_array:
		element['comment'] = getCommentLowercase(element['comment'])
		writer.write('{"directors":'+str(element['directors'])+
					 ',"nationalities":'+str(element['nationalities'])+
					 ',"rate":'+str(element['rate'])+
					 ',"comment":"'+element['comment']+
					 '","review_id":"'+element['review_id']+
					 '","username":'+str(element['username'])+
					 ',"genres":'+str(element['genres'])+'}\n')

def isEmoji(character):
	''' (String) -> Boolean

	isEmoji("🎬")
	>>> True
	'''
	return character in UNICODE_EMOJI

def getCommentWithoutEmojis(comment):
	''' (String) -> String

	getCommentWithoutEmojis("Amateur de film de possession, j'ai été extrêmement déçu. 🎬")
	>>> "Amateur de film de possession, j'ai été extrêmement déçu."
	'''
	emojis = []
	for character in comment:
		if isEmoji(character) and character not in emojis:
			emojis.append(character)
	for emoji in emojis:
		comment = getCommentWithout(comment, emoji)
	return comment

def deleteSmileysVerification(comment, smiley):
	''' (String, String) -> String

	deleteSmileysVerification("Plus le même réalisateur =plus les mêmes acteurs = suite d'un premier film souvent désastreux =p", "=p")
	>>> "Plus le même réalisateur  =plus les mêmes acteurs = suite d'un premier film souvent désastreux"
	'''
	commentParts = comment.split(smiley)
	comment = ""
	for index in range(len(commentParts)):
		if index == len(commentParts)-1:
			comment += commentParts[index]
		else:
			comment += commentParts[index]
			if commentParts[index+1] != "" and commentParts[index+1][0] != ' ' and commentParts[index+1][0].islower():
				comment += smiley
	return comment

def deleteSmileys(comment, smileys):
	''' (String, list of string) -> String

	deleteSmileys("tous ceux qui ont un coeur de midinette. ^^", ['^^', "^^'"])
	>>> "tous ceux qui ont un coeur de midinette. "
	'''
	specials = [':', ';', '=', 'x', 'X']
	for smiley in smileys:
		if smiley[0] in specials:
			comment = deleteSmileysVerification(comment, smiley)
		else:
			comment = getCommentWithout(comment, smiley)
	return comment

def processWithoutSmileys(dict_path, output_file_path):
	print('Without smileys')
	file = open(dict_path, 'r')
	content_string = file.read()
	content_array = [eval(object) for object in content_string.split('\n')[:-1]]
	file.close()
	writer = open(output_file_path, 'w')
	for element in content_array:
		element['comment'] = getCommentWithoutSmileys(element['comment'])
		writer.write('{"directors":'+str(element['directors'])+
					 ',"nationalities":'+str(element['nationalities'])+
					 ',"rate":'+str(element['rate'])+
					 ',"comment":"'+element['comment']+
					 '","review_id":"'+element['review_id']+
					 '","username":'+str(element['username'])+
					 ',"genres":'+str(element['genres'])+'}\n')

def getCommentWithoutPonctuation(comment):
	comment = getCommentWithout(comment, '!')
	comment = getCommentWithout(comment, '?')
	comment = getCommentWithout(comment, '…')
	comment = getCommentWithout(comment, '.') # .. ... .... .....
	comment = getCommentWithoutSpace(comment)
	return comment

def processWithoutPonctuation(dict_path, output_file_path):
	print('Without ponctuation')
	file = open(dict_path, 'r')
	content_string = file.read()
	content_array = [eval(object) for object in content_string.split('\n')[:-1]]
	file.close()
	writer = open(output_file_path, 'w')
	for element in content_array:
		element['comment'] = getCommentWithoutPonctuation(element['comment'])
		writer.write('{"directors":'+str(element['directors'])+
					 ',"nationalities":'+str(element['nationalities'])+
					 ',"rate":'+str(element['rate'])+
					 ',"comment":"'+element['comment']+
					 '","review_id":"'+element['review_id']+
					 '","username":'+str(element['username'])+
					 ',"genres":'+str(element['genres'])+'}\n')

def getCommentClean(comment):
	comment = unidecode.unidecode(comment)
	comment = re.sub('[^a-zA-Z]', ' ', comment)
	comment = re.sub(r'\s+', ' ', comment)
	comment = getCommentLowercase(comment)
	comment = getCommentWithoutSpace(comment)
	return comment

def processClean(dict_path, output_file_path):
	print('Clean')
	file = open(dict_path, 'r')
	content_string = file.read()
	content_array = [eval(object) for object in content_string.split('\n')[:-1]]
	file.close()
	writer = open(output_file_path, 'w')
	for element in content_array:
		element['comment'] = getCommentClean(element['comment'])
		writer.write('{"directors":'+str(element['directors'])+
					 ',"nationalities":'+str(element['nationalities'])+
					 ',"rate":'+str(element['rate'])+
					 ',"comment":"'+element['comment']+
					 '","review_id":"'+element['review_id']+
					 '","username":'+str(element['username'])+
					 ',"genres":'+str(element['genres'])+'}\n')

if __name__ == '__main__':

	# Create movies dict and movies urls
	#createMoviesDictAndURLS("../../Data/Raw/dev.xml", "../../Data/Raw/train.xml", "../../Data/Raw/test.xml", "../../Data/Json/dict_movies.json", "../../Data/TXT/movies_urls.txt")

	# Fill movies dict
	#fillMoviesDict("../../Data/Json/dict_movies.json", "../../Data/Urls/www.allocine.fr/film/")

	# Kibana
	# Dev : xml => json
	#processKibana("../../Data/XML/dev.xml", "../../Kibana/Json/Dev.json", "dev", "../../Data/Json/dict_movies.json")

	# Train : xml => json
	#processKibana("../../Data/XML/train.xml","../../Kibana/Json/Train.json", "train", "../../Data/Json/dict_movies.json")

	# Split Dev.json and Train.json files
	#subprocess.call(['sh', '../split_files.sh'])

	# Now verify each files to not begin or stop in the middle of a comment. Check the '../../Kibana/Json/Dev_part0.json' file for example.

	# Integrate into Kibana
	#subprocess.call(['sh', '../integrateToKibana.sh'])

	# SVM
	# Train, Dev & Test : xml => svm, with smileys
	#xmlToSVM("../../Data/XML/train.xml", "../../Data/XML/dev.xml", "../../Data/XML/test.xml", "../../Data/SVM/Train/", "../../Data/SVM/Dev/", "../../Data/SVM/Test/", "With")

	# Train, Dev & Test : xml => svm, without smileys
	#xmlToSVM("../../Data/XML/train.xml", "../../Data/XML/dev.xml", "../../Data/XML/test.xml", "../../Data/SVM/Train/", "../../Data/SVM/Dev/", "../../Data/SVM/Test/", "Without")

	# sh code to train and predict with liblinear
	#subprocess.call(['sh', '../svm.sh'])

	# Test : txt => good_format.txt, with smileys
	#resultToGoodFormat("../../Logs/Results/SVM/with_smileys.txt", "../../Data/XML/test.xml", "../../Logs/Results/SVM/gf_with_smileys.txt")

	# Test : txt => good_format.txt, without smileys
	#resultToGoodFormat("../../Logs/Results/SVM/without_smileys.txt", "../../Data/XML/test.xml", "../../Logs/Results/SVM/gf_without_smileys.txt")

	# JSON Files
	# Create directors.txt, nationalities.txt, genres.txt
	#createDirectorsNationalitiesGenresFiles("../../Data/Json/dict_movies.json", "../../Data/Txt/directors.txt", "../../Data/Txt/nationalities.txt", "../../Data/Txt/genres.txt")

	# Create usernames.txt
	#createUsernamesFile("../../Data/XML/train.xml", "../../Data/XML/dev.xml", "../../Data/XML/test.xml", "../../Data/TXT/usernames.txt")

	# Baseline Test
	#processBaseline("../../Data/XML/test.xml", "../../Data/Json/Test/baseline.json", "../../Data/Json/dict_movies.json", "../../Data/Txt/directors.txt", "../../Data/Txt/nationalities.txt", "../../Data/Txt/usernames.txt", "../../Data/Txt/genres.txt")

	# Baseline Train
	#processBaseline("../../Data/XML/train.xml", "../../Data/Json/Train/baseline.json", "../../Data/Json/dict_movies.json", "../../Data/Txt/directors.txt", "../../Data/Txt/nationalities.txt", "../../Data/Txt/usernames.txt", "../../Data/Txt/genres.txt")

	# Baseline Dev
	#processBaseline("../../Data/XML/dev.xml", "../../Data/Json/Dev/baseline.json", "../../Data/Json/dict_movies.json", "../../Data/Txt/directors.txt", "../../Data/Txt/nationalities.txt", "../../Data/Txt/usernames.txt", "../../Data/Txt/genres.txt")

	# Lowerize Test
	#processLowerize("../../Data/Json/Test/baseline.json", "../../Data/Json/Test/lowercase.json")

	# Without_smileys Test
	#processWithoutSmileys("../../Data/Json/Test/baseline.json", "../../Data/Json/Test/without_smileys.json")

	# Without ponctuation Test
	#processWithoutPonctuation("../../Data/Json/Test/baseline.json", "../../Data/Json/Test/without_ponctuation.json")

	# Clean Test
	#processClean("../../Data/Json/Test/baseline.json", "../../Data/Json/Test/clean.json")

	# Lowerize Train
	#processLowerize("../../Data/Json/Train/baseline.json", "../../Data/Json/Train/lowercase.json")

	# Without_smileys Train
	#processWithoutSmileys("../../Data/Json/Train/baseline.json", "../../Data/Json/Train/without_smileys.json")

	# Without ponctuation Train
	#processWithoutPonctuation("../../Data/Json/Train/baseline.json", "../../Data/Json/Train/without_ponctuation.json")

	# Clean Train
	#processClean("../../Data/Json/Train/baseline.json", "../../Data/Json/Train/clean.json")

	# Lowerize Dev
	#processLowerize("../../Data/Json/Dev/baseline.json", "../../Data/Json/Dev/lowercase.json")

	# Without_smileys Dev
	#processWithoutSmileys("../../Data/Json/Dev/baseline.json", "../../Data/Json/Dev/without_smileys.json")

	# Without ponctuation Dev
	#processWithoutPonctuation("../../Data/Json/Dev/baseline.json", "../../Data/Json/Dev/without_ponctuation.json")

	# Clean Dev
	#processClean("../../Data/Json/Dev/baseline.json", "../../Data/Json/Dev/clean.json")
