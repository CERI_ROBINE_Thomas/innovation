#-*- coding: utf-8 -*-

from gensim.models import Word2Vec
from gensim.models.fasttext import FastText
import json
from Json import load_json, get_words

# Training the Word2Vec model
def train_word2vec(comments):
    # Train
    word2vec = Word2Vec(comments, size=100, min_count=10, window=5)
    # Test
    print(word2vec.wv['good'])
    print(word2vec.wv.most_similar('super'))
    print(word2vec.wv.most_similar('film'))
    print(word2vec.wv.most_similar('nul'))
    return word2vec

# Training the FastText model
def train_fasttext(comments):
    # Train
    fasttext = FastText(comments, size=100, window=5, min_count=10)
    # Test
    print(fasttext.wv['good'])
    print(fasttext.wv.most_similar('super'))
    print(fasttext.wv.most_similar('film'))
    print(fasttext.wv.most_similar('nul'))
    return fasttext

# Get dictionnary from vocabulary to have all objects vectors
def get_dict(word2vec,words):
    dict = {}
    for word in words:
        try:
            vec = word2vec.wv[word]
            dict[word] = str(list(vec))
        except KeyError:
            continue
    return dict

# Save it one time to use it many times
def save_vocabulary():
    for version_name in ['baseline', 'clean']:
        # Get data
        comments, *_ = load_json(version_name, ['Train', 'Dev', 'Test'])
        words = get_words(comments)
        # Get vocabulary
        word2vec = train_word2vec(comments)
        fasttext = train_fasttext(comments)
        # Save it in json file
        file = open('../../Data/Json/Word2Vec_' + version_name + '_100.json', 'w')
        dict = get_dict(word2vec, words)
        json.dump(dict,file)
        file.close()
        file = open('../../Data/Json/FastText_' + version_name + '_100.json', 'w')
        dict = get_dict(fasttext, words)
        json.dump(dict,file)
        file.close()

# Load saved json file to preprocess actual data
def load_vectors(preprocessing_method, vectors_size, version_name):
    file = open('../../Data/Json/' + preprocessing_method + '_' + version_name + '_' + str(vectors_size) + '.json', 'r')
    content = file.read()
    dict = json.loads(content)
    file.close()
    return dict

#load_vectors('Word2Vec', 100, 'baseline')
#save_vocabulary()
