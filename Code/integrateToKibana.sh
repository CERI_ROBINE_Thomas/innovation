#!bin/bash

cd ../Mapping/
curl -X PUT "localhost:9200/data_movie_dev" -H 'Content-Type: application/json' -d "@mapping_movie_train_dev.json"
curl -X PUT "localhost:9200/data_movie_train" -H 'Content-Type: application/json' -d "@mapping_movie_train_dev"
cd ../Kibana/Json/
curl -H 'Content-Type: application/n-ndjson' -XPOST 'localhost:9200/data_movie_dev/_bulk?pretty' --data-binary @Dev_part0.json
curl -H 'Content-Type: application/n-ndjson' -XPOST 'localhost:9200/data_movie_dev/_bulk?pretty' --data-binary @Dev_part1.json
curl -H 'Content-Type: application/n-ndjson' -XPOST 'localhost:9200/data_movie_dev/_bulk?pretty' --data-binary @Dev_part2.json
curl -H 'Content-Type: application/n-ndjson' -XPOST 'localhost:9200/data_movie_train/_bulk?pretty' --data-binary @Train_part00.json
curl -H 'Content-Type: application/n-ndjson' -XPOST 'localhost:9200/data_movie_train/_bulk?pretty' --data-binary @Train_part01.json
curl -H 'Content-Type: application/n-ndjson' -XPOST 'localhost:9200/data_movie_train/_bulk?pretty' --data-binary @Train_part02.json
curl -H 'Content-Type: application/n-ndjson' -XPOST 'localhost:9200/data_movie_train/_bulk?pretty' --data-binary @Train_part03.json
curl -H 'Content-Type: application/n-ndjson' -XPOST 'localhost:9200/data_movie_train/_bulk?pretty' --data-binary @Train_part04.json
curl -H 'Content-Type: application/n-ndjson' -XPOST 'localhost:9200/data_movie_train/_bulk?pretty' --data-binary @Train_part05.json
curl -H 'Content-Type: application/n-ndjson' -XPOST 'localhost:9200/data_movie_train/_bulk?pretty' --data-binary @Train_part06.json
curl -H 'Content-Type: application/n-ndjson' -XPOST 'localhost:9200/data_movie_train/_bulk?pretty' --data-binary @Train_part07.json
curl -H 'Content-Type: application/n-ndjson' -XPOST 'localhost:9200/data_movie_train/_bulk?pretty' --data-binary @Train_part08.json
curl -H 'Content-Type: application/n-ndjson' -XPOST 'localhost:9200/data_movie_train/_bulk?pretty' --data-binary @Train_part09.json
curl -H 'Content-Type: application/n-ndjson' -XPOST 'localhost:9200/data_movie_train/_bulk?pretty' --data-binary @Train_part10.json
curl -H 'Content-Type: application/n-ndjson' -XPOST 'localhost:9200/data_movie_train/_bulk?pretty' --data-binary @Train_part11.json
curl -H 'Content-Type: application/n-ndjson' -XPOST 'localhost:9200/data_movie_train/_bulk?pretty' --data-binary @Train_part12.json
curl -H 'Content-Type: application/n-ndjson' -XPOST 'localhost:9200/data_movie_train/_bulk?pretty' --data-binary @Train_part13.json
curl -H 'Content-Type: application/n-ndjson' -XPOST 'localhost:9200/data_movie_train/_bulk?pretty' --data-binary @Train_part14.json
curl -H 'Content-Type: application/n-ndjson' -XPOST 'localhost:9200/data_movie_train/_bulk?pretty' --data-binary @Train_part15.json
curl -H 'Content-Type: application/n-ndjson' -XPOST 'localhost:9200/data_movie_train/_bulk?pretty' --data-binary @Train_part16.json
curl -H 'Content-Type: application/n-ndjson' -XPOST 'localhost:9200/data_movie_train/_bulk?pretty' --data-binary @Train_part17.json
