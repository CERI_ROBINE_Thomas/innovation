# Innovation : Prediction of movie notes
Master 2 Innovation Challenge from Innovation UE at CERI. This challenge is about creating an AI able to predict a movie's review note.
The corpus used in this project is a part of all the reviews that can be found on https://www.allocine.fr/ . 
This way, we implemented SVM, CNN and DNN approches to train on the comments, with bag of words or words embeddings (FastText and Word2Vec). 

## Technologies and frameworks used
All the ``.py`` scripts are written in <i>Python 3.7.4 (July 8, 2019)</i>.
The libraries used for this project are as follows:

- [Python 3.7.4](https://www.python.org)
- [Scikit-Learn 0.21.3](https://scikit-learn.org)
- [CudaToolkit 10.1.243](https://developer.nvidia.com/cuda-toolkit) to use cuda devices
- [Numpy 1.19.2](https://numpy.org/doc/) to process data
- [PyTorch 1.4.0](https://pytorch.org) to train DNN and CNN models
- [TensorBoard 1.14.0](https://pytorch.org/docs/stable/tensorboard.html) to visualize metrics and loss for Pytorch
- [Gensim 3.8.0](https://pypi.org/project/gensim/) to use Wav2Vec and FastText models
- [Keras 2.3.1](https://keras.io) to make one-hot labels when pre-processing for Pytorch
- [Nltk 3.5](https://www.nltk.org) to tokenize data when pre-processing for Pytorch
- [Unidecode 1.1.2](https://pypi.org/project/Unidecode/) to clean data when pre-processing for Pytorch
- [Logging 0.4.9.6](https://pypi.org/project/logging/) to print logs in general
- [Emoji 0.6.0](https://pypi.org/project/emoji/) to know if a character in a comment is an emoji
- [ElasticSearch 7.10.0](https://www.elastic.co/fr/downloads/elasticsearch) to visualize data with Kibana
- [Kibana 7.10.1](https://www.elastic.co/fr/downloads/kibana) to visualize data
- [Liblinear 242](https://github.com/cjlin1/liblinear) to use the SVM

You can install all those libraries through python's package manager ``pip`` but preferably with ``conda``. Conda will sometimes need a channel ``-c conda-forge``.

```
pip install library
conda install library
```

Except for [Kibana](https://www.elastic.co/fr/downloads/kibana) and [ElasticSearch](https://www.elastic.co/fr/downloads/elasticsearch) that you can download from the ElsaticSearch site and [Liblinear](https://github.com/cjlin1/liblinear) that you can take from the Github repository.

If you can not use ``ElasticSearch`` after downloading it from ElasticSaerch website, you can install it with the ``apt-get install`` command and run it with ``service elasticsearch start``.

```
apt-get install elasticsearch
service elasticsearch start
```

## Structure

The repository is divided in few directories. Firstly, for the scripts :

- ``Code`` has all the code needed for our models to run.
- ``Code/Liblinear`` is the automaticaly generated lib for Kibana. 
- ``Code/Pytorch`` is the directory for DNN and CNN.
- ``Code/Preprocess`` is the directory for all data related preprocessing
- Plus ``svm.sh`` script to run SVM.

For the datasets :

- ``Data`` has all preprocess, for different tasks. 
- ``Data/XML`` directory contain original data. 
- ``Data/Txt`` and ``Data/URLS`` have files used to get meta-data.  
- ``Data/Json`` store initial pre-processes with meta-data.
- ``Data/SVM`` contains the data format needed for SVM models, so does ``Data/Pickle`` for Pytorch models. 

For data exploration :

- ``Kibana`` contains all related to data exploration.
- ``Kibana/Json`` is filled of Kibana format to do so. 
- ``Kibana/Plots`` has the arborescence of the different plots we made with Kibana. 

For logs when training : 

- ``Logs`` contain all the information for Pytorch models. 
- ``Logs/Parameters`` has textual files of the experiment informations detailed, following a training date. 
- ``Logs/TensorBoard`` has the TensorBoard directories, following the same date.
- ``Logs/Results`` has the textual files for testing on the [dedicated web site](http://mickael-rouvier.fr/eval_2020/index.php), following the same date.
- Plus ``Logs/results.txt`` file to keep track of those tests. 

For models save :

- ``Models`` contains the checkpoints of our experimentations on CNN and DNN with pytorch, following the same date.

## Kibana

You must uncomment the code from line 1175 to 1186 include, in ``Code/Preprocess/preprocessGlobal.py``. Next, you must follow the instructions provided in this file. Then run :

```
python preprocessGlobal.py
```

## SVM

You must uncomment the code from line 1190 to 1202  include, in ``Code/Preprocess/preprocessGlobal.py``. Next, you must follow the instructions provided in this file. Then run :

```
python preprocessGlobal.py
```

## Pre-processing for Pytorch

### Parameters

- ``batch_size`` is the size of a batch given to our model
- ``vectors_size`` is the number of coefficient for each word embedding
- ``version_name`` is the dataset versions possible to pre-process
- ``preprocessing_method`` is either Word2Vec or FastText
- ``input_dimension``is the number of words per comment chunked (a chunk size if you prefer)

### Gensim.py 
First, before using preprocessing many times, you need to build Word2Vec and FastText vocabularies and store them in json format. To do so, you need to uncomment the last two lines of ``Gensim.py`` and run it, before commenting them again (because the script wiil be used latter for training). 

```
python Gensim.py
```

Please choose a vectors size of 100 for better results and more precision. 
``TFIDF.py`` is also executable the same way, but we figured out we didn't need it. We pushed it in the repository for future evolutions, when it can be needed. 

### preprocessPytorch.py
To preprocess all data needed before training with Pytorch, you will have to execute the ``snipsPreprocess.py``. You will need some hard disk space to do so: approximately 4 To, only if you decide to preprocess all datasets at a time. If so, it will take a lot of time. Prefer running the script for one precise dataset, train on it, and try another one. 

```
python preprocessPytorch.py
```

As said, it will pre-process data and save it with Pickle, in ``Data/Pickle`` directory. The data dumped into pickle files will be lists of batches of: inputs, outputs (one-hot) and review IDs. We decided to fix the batch size to 100 and the input size (lenght of comments) to 200, by chunking the too long ones. It allowed us to use ``np.wrap`` instead of a padding with zeros, to improove our trainings. This command padd the comments with themselves. 
As well, the embedding vectors size is fixed to 100. After looking at the similarities of those vectors, with FastText and Word2Vec, we found that a lenght of 100 was better. Lower lenghts may occur ``film`` or ``boring`` to have much similarity with non-related terms. 

## DNN and CNN with Pytorch

### Parameters

For the data :

- ``version_name`` as said before
- ``preprocessing_method`` as said before
- ``vectors_size`` is the number of coefficient for each word embedding : 100 here 
- ``input_dimension`` is the dimension of our chunks of comments : 200 here
- ``output_dimension`` is the dimension of our classification : 10 fixed

For the optimization :

- ``str_NN`` is meant to change the type of NN used : ``CNN`` or ``DNN``
- ``str_optimizer`` is meant to keep track of the optimizer used, to update when changing optimizer in ``snipsModel.py``
- ``str_loss`` is meant to keep track of the loss used, to update when changing loss in ``snipsModel.py``
- ``str_metrics`` is meant to keep track of the metrics used : Accuracy fixed
- ``nb_epochs`` is the number of epochs to train the model
- ``nb_neurones`` is the array of the number of neurons for each hidden layer
- ``dropouts`` is the array of the dropout's values for each hidden layer
- ``learning_rate``
- ``momentum`` not used if Adam optimizer, used if SGD optimizer

You can also use a checkpoint by setting ``use_checkpoint = True`` and define an old model date in the parameters instead of ``date = ''``. 

Documentation :

- [losses, optimizers, metrics, activation functions, etc.](https://pytorch.org/docs/stable/nn.html)
- [CNN](https://pytorch.org/docs/master/generated/torch.nn.Conv2d.html#conv2d)

### main.py
To run the software you will only have execute ``main.py`` and activate your environment.
You don't need to create dictionaries for models or logs. The script does it for you.

```
python main.py
```

It will create the model by using train and validation datasets. It uses a checkpoint after each improvement of the model. This checkpoint is located in ``Models`` directory and is named after the date and time of it creation (``Year Month Day - Hour Minutes Seconds``). The other callback used by the model save the logs for a TensorBoard visualization in the ``Logs/Tensorboard/`` directory. Tu run Tensorboard, please open a console in the ``Logs/Tensorboard`` directory and write this command (you can change the port): 

```
tensorboard --logdir=. --port=6666
```

Another log file is in ``Logs/Parameters``. It contains the optimization parameters of the dated model. Finaly, you can get your inference resulting file in ``Logs/Result`` to use it on the [dedicated web site](http://mickael-rouvier.fr/eval_2020/index.php). 
